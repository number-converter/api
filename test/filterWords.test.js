const fs = require('fs')
const path = require('path')
const assert = require('assert')
const convertNumber = require('../helpers/convertNumber')

describe('Convert Numbers to Array of corresponding words in the style of T9', () => {
	const dictionary = fs.readFileSync(path.basename('files') + '/dictionary.txt', {
		encoding: 'utf-8'
	})

	const dictionaryArray = dictionary.split('\n')

	const filterValues = (values) => values.filter((val) => dictionaryArray.includes(val))

	it('should return one letter word', () => {
		const words = convertNumber('2')

		const res = filterValues(words)

		assert.equal(
			'a',
			res.find((val) => val === 'a')
		)
	})

	it('should return two letter word', () => {
		const words = convertNumber('23')

		const res = filterValues(words)

		assert.equal(
			'be',
			res.find((val) => val === 'be')
		)
	})

	it('should return three letter word', () => {
		const words = convertNumber('569')

		const res = filterValues(words)

		assert.equal(
			'joy',
			res.find((val) => val === 'joy')
		)
	})

	it('should return word kiwi', () => {
		const words = convertNumber('5494')

		const res = filterValues(words)

		assert.equal(
			'kiwi',
			res.find((val) => val === 'kiwi')
		)
	})
})
