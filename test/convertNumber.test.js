const assert = require('assert')
const { MAX_INPUT_LENGTH } = require('../constants/base')
const convertNumber = require('../helpers/convertNumber')

describe('Convert Numbers to Array of corresponding words in the style of T9', () => {
	it('should return empty array for type number', () => {
		const res = convertNumber(43)
		const res2 = convertNumber(3345345)

		assert.equal(res.length, 0)
		assert.equal(res2.length, 0)
		assert.deepEqual(res, [])
		assert.deepEqual(res2, [])
	})

	it('should return empty array for empty string', () => {
		const res = convertNumber('')

		assert.equal(res.length, 0)
		assert.deepEqual(res, [])
	})

	it('should return empty array if entry is large than max size', () => {
		const res = convertNumber('12345678910')

		assert.equal(res.length, 0)
		assert.deepEqual(res, [])
	})

	it('should convet 1 number', () => {
		const res = convertNumber('1')
		const res2 = convertNumber('2')

		assert.deepEqual(res, [])
		assert.deepEqual(res2, ['a', 'b', 'c'])
	})

	it('should convet 2 numbers', () => {
		const res = convertNumber('23')
		const res2 = convertNumber('02')
		const res3 = convertNumber('00')

		assert.deepEqual(res, ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf'])
		assert.deepEqual(res2, ['a', 'b', 'c'])
		assert.deepEqual(res3, [])
	})

	it('should convet 3 numbers', () => {
		const res = convertNumber('123')
		const res2 = convertNumber('234')

		assert.deepEqual(res, ['ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf'])
		assert.equal(res2.length, Math.pow(3, 3))
	})

	it(`should convet ${MAX_INPUT_LENGTH} numbers`, () => {
		const res = convertNumber('4676774253')
		const res2 = convertNumber('2342342342')

		assert.equal(
			'impossible',
			res.find((val) => val === 'impossible')
		)
		assert.equal(res2.length, Math.pow(3, 10))
	})

	it('should contain word: "kiwi" and "Mirec".', () => {
		const kiwi = convertNumber('5494')
		const mirec = convertNumber('64732')

		assert.equal(
			'kiwi',
			kiwi.find((val) => val === 'kiwi')
		)

		assert.equal(
			'mirec',
			mirec.find((val) => val === 'mirec')
		)
	})
})
