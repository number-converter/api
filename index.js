const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const appRoutes = require('./routes/routes')

const app = express()
const PORT = 5000

app.use(bodyParser.json())
app.use(cors())

app.use('/api/v1', appRoutes)

app.listen(PORT, () => {})
