const fs = require('fs')
const path = require('path')
const STATUS_CODES = require('../constants/statusCodes')
const convertNumber = require('../helpers/convertNumber')
const { ERRORS } = require('../constants/errors')

exports.getNumberConvertResults = async (req, res) => {
	const { number: numberValue } = req.params

	try {
		const values = convertNumber(numberValue)

		const dictionary = fs.readFileSync(path.basename('files') + '/dictionary.txt', {
			encoding: 'utf-8'
		})

		const dictionaryArray = dictionary.split('\n')

		const filteredValues = values.filter((val) => dictionaryArray.includes(val))

		res.status(STATUS_CODES.OK).json({
			results: {
				values: filteredValues,
				count: filteredValues.length
			},
			message: 'ok',
			statusCode: STATUS_CODES.OK
		})
	} catch (error) {
		res.status(STATUS_CODES.SERVER_ERROR).json({
			message: ERRORS.FAILED,
			statusCode: STATUS_CODES.SERVER_ERROR
		})
	}
}
