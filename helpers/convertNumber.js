const { MAX_INPUT_LENGTH } = require('../constants/base')

module.exports = (number) => {
	if (!number || !number.length || number.length > MAX_INPUT_LENGTH) {
		return []
	}

	const numbersArray = number.split('')

	const KEYBORD_VALUES = {
		1: [],
		2: ['a', 'b', 'c'],
		3: ['d', 'e', 'f'],
		4: ['g', 'h', 'i'],
		5: ['j', 'k', 'l'],
		6: ['m', 'n', 'o'],
		7: ['p', 'q', 'r', 's'],
		8: ['t', 'u', 'v'],
		9: ['w', 'x', 'y', 'z'],
		0: []
	}

	const arrayOfLettersCombinations = numbersArray
		.map((num) => KEYBORD_VALUES[num])
		.filter((val) => val.length > 0)

	const result =
		arrayOfLettersCombinations.length > 0
			? arrayOfLettersCombinations.reduce((acc, letters) => {
					const words = []

					acc.map((curr) => {
						return [...letters].map((letter) => words.push(`${curr}${letter}`))
					})

					return words
			  })
			: []

	return result
}
