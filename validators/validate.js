const { validationResult } = require('express-validator')
const { ERRORS } = require('../constants/errors')
const STATUS_CODES = require('../constants/statusCodes')

const validate = (req, res, next) => {
	const errors = validationResult(req)

	if (errors.isEmpty()) {
		return next()
	}

	const errArr = errors.array()
	const { msg = {} } = errArr[0] || {}

	const { message = ERRORS.NOT_VALID, statusCode = STATUS_CODES.UNPROCESSABLE_ENTITY } = msg

	return res.status(statusCode).json({
		message,
		statusCode
	})
}

module.exports = {
	validate
}
