const { ERRORS } = require('../constants/errors')
const { check } = require('express-validator')
const STATUS_CODES = require('../constants/statusCodes')
const { MAX_INPUT_LENGTH } = require('../constants/base')

exports.numberValidatorRules = () => {
	return [
		check('number', { message: ERRORS.NOT_VALID, statusCode: STATUS_CODES.UNPROCESSABLE_ENTITY })
			.notEmpty()
			.isString()
			.isLength({
				max: MAX_INPUT_LENGTH
			})
			.custom(async (value) => {
				const isNotNumber = isNaN(value)

				if (isNotNumber) {
					return Promise.reject({
						message: ERRORS.NOT_VALID,
						statusCode: STATUS_CODES.UNPROCESSABLE_ENTITY
					})
				}

				return true
			})
	]
}
