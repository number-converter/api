const express = require('express')
const NumberValidator = require('../validators/NumberValidator')
const NumberController = require('../controllers/NumberController')
const { validate } = require('../validators/validate')

const router = express.Router()

router.get(
	'/results/:number',
	NumberValidator.numberValidatorRules(),
	validate,
	NumberController.getNumberConvertResults
)

module.exports = router
