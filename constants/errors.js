const ERRORS = Object.freeze({
	NOT_VALID: 'not_valid',
	FAILED: 'failed'
})

module.exports = {
	ERRORS
}
